import Vue from 'vue'
import VueRouter from 'vue-router'
import {auth} from '../firebase';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: () => import(/* webpackChunkName: "about" */ '../views/Inicio.vue'),
    meta: {requiresAuth: true}
  },
  {
    path: '/registro',
    name: 'Registro',
    component: () => import(/* webpackChunkName: "about" */ '../views/Registro.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach((to,form,next)=>{

  if(to.matched.some(record => record.meta.requiresAuth)){
    
    if(auth.currentUser){
       next();
    }else{
      next({path: '/login'});
    }

  }else{
     next();
  }

})

export default router
