import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBRyORTNNgQTj05eJNJblUQMc3Nquqt7Jw",
    authDomain: "auth-vue-f4ddb.firebaseapp.com",
    projectId: "auth-vue-f4ddb",
    storageBucket: "auth-vue-f4ddb.appspot.com",
    messagingSenderId: "653842875634",
    appId: "1:653842875634:web:71782dc6e1ac2888493c6b"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();

export {
    db,
    auth
}