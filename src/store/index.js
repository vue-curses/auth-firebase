import Vue from 'vue'
import Vuex from 'vuex'
import {auth,db} from '../firebase';
import router from '../router';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usuario: null,
    error: null
  },
  mutations: {
    setUsuario(state, payload){
      state.usuario = payload;
    },
    setError(state,payload){
      state.error = payload;
    }
  },
  actions: {
    crearUsuario({commit},usuario){
      const {email, password} = usuario;
      auth.createUserWithEmailAndPassword(email, password)
      .then(res =>{
        const usuarioCreado ={
          email: res.user.email,
          uid: res.user.uid
        }
        commit('setUsuario', usuarioCreado);
        router.push('/');
      })
      .catch(error => commit('setError', error));
    },
    ingresoUsuario({commit}, usuario){
      const {email, password} = usuario;
      auth.signInWithEmailAndPassword(email, password)
      .then(res => {
        const usuarioLogeado ={
          email: res.user.email,
          uid: res.user.uid
        }
        commit('setUsuario', usuarioLogeado);
        router.push('/');
      })
      .catch(error => commit('setError', error))
    },
    cerrarSesion({commit}){
      auth.signOut()
      .then(()=>{
        commit('setUsuario', null);
        router.push('/login');
      })
      .catch(error => commit('setError', error))
    },
    detectarUsuario({commit}, usuario){
      commit('setUsuario', usuario);
    }
  },
  getters:{
     userExist(state){
       return state.usuario === null ? false : true;
     }
  },
  modules: {
  }
})
